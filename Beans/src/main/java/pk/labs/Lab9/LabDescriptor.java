package pk.labs.Lab9;

import pk.labs.Lab9.beans.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = new pk.labs.Lab9.beans.impl.Termin().getClass();
    public static Class<? extends Consultation> consultationBean = new pk.labs.Lab9.beans.impl.Konsultacja().getClass();
    public static Class<? extends ConsultationList> consultationListBean = new pk.labs.Lab9.beans.impl.ListaKonsultacji().getClass();
    public static Class<? extends ConsultationListFactory> consultationListFactory = new pk.labs.Lab9.beans.impl.Fabryka().getClass();
    
}
